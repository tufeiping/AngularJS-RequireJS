require.config({

    // alias libraries paths
    paths: {
        'domReady': '../libs/requirejs-domready/domReady',
        'angular': '../libs/angular/angular.min',
        'uiRouter': '../libs/angular-ui-router/release/angular-ui-router.min',
        'uiBootstrap': '../libs/angular-bootstrap/ui-bootstrap-tpls.min',
        'ngResource': '../libs/angular-resource/angular-resource.min'
    },
    // angular does not support AMD out of the box, put it in a shim
    shim: {
        'angular': {
            exports: 'angular'
        },
        'uiRouter': {
            deps: ['angular']
        },
        'ngResource': {
            deps: ['angular']
        },
        'uiBootstrap': {
            deps: ['angular']
        }
    },

    // kick start application
    deps: ['./bootstrap']
});